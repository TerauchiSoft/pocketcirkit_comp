プログラムの実行方法

*** エミュレーター g800 を使用する場合 ***

1.オールリセット
デフォルトではF8キー

2.hexファイルの適用
BASIC画面で
MON returnキーでマシン語モニタに入り
USER 76E1 returnキー

次に
F4(menu)
S(sio)
R(read)
ファイル選択(tabキーでカレントディレクトリをひとつ遡れる)
enterキーで目的のファイル(.hex)を選択すると、READ OK:0100 - 76D7と表示される。
RUN MODE画面に戻って、CALL256 or CALL &H100 returnキー でゲーム開始。

*** 実機(pc-g850[-,s,v,vs])を使用する場合 ***

1.オールリセット
ポケコン左下のリセットボタンを押してリセット

2.hexファイルの送信
TEXTボタン
S
F
転送速度を最適なものに設定して  returnキー
CLSキーを押す

3.ポケコンに転送
BASIC画面で
MON returnキーを押してマシン語モニタに入り
USER 76E1 returnキー
R returnキーで通信待機

転送ソフトでポケコンにデータ送信を開始
しばらく待って
INFO 0100-76E1と表示される。
RUN MODE画面に戻って、CALL256 or CALL &H100 returnキー でゲーム開始。
